# Основные принципы построения моделей

Вы всё ещё здесь? Значит уже освоили интерфейс и настроили Blender? Умеете добавлять объекты? Переходить по разным режимам? Отлично! Включайте музыку и творите! Ведь с этого момента начинается то, за чем вы сюда пришли.
***

## Подготовка пространства и самого себя
Выделите и удалите(del) камеру и свет со сцены. Просто чтобы они не мешали моделированию...

Нажмите на куб и перейдите в **режим редактирования** нажав на **Tab**. Вы теперь видите вершины.

![Вершины куба](./insertions/cube-vertex.png)
***

## Выделение вершин, рёбер, граней
Давайте научимся пользоваться выделением. Задача: вам необходимо выбрать две противоположных вершины так, чтобы каждая из них была выделена.

Это сделать довольно просто: нажмите на первую вершину (**ЛКМ**), затем нажмите на вторую и последующие вершины так же, но с зажатой клавишей Shift (**Shift + ЛКМ**).

Убирать выделение с определённых вершин происходит как в случае выбора второй вершины (**Shift + ЛКМ**).

![Выделение вершин](./insertions/excretions.gif)

То же самое можно провернуть и с рёбрами, и с гранями, однако сперва необходимо перейти в их пространство. Нажмите клавишу **2** и окажетесь в пространстве редактирования рёбер, **3** - граней. Соответственно **1** - вершин.

Примечание:
- В пространстве редактирования рёбер и граней вы не увидите вершин.
***

## Выдавливание (Extrude)
Выдавливание — это метод добавления геометрии (*в нашем случае вершин*) дублируя и соединяя её с первоначальной геометрией. 

Вот так выглядит выдавливание:

![Выдавливание](./insertions/extrude.gif)

Довольно простая, но очень полезная вещь. По сути ни один вид моделирования не обходится без выдавливания.

За выдавливание отвечает клавиша **E** (*англ.*) на клавиатуре.
***

## Вставка (Insert)
Позволяет добавить вершины на уже существующую грань. 

**Важно**, чтобы перед вставкой вы выбрали одну или несколько **граней**, если вы выберете несоединённые вершины или ребра, вставка просто не сработает!

Вот так выглядит вставка:

![Вставка](./insertions/insert.gif)

Делается вставка нажатием клавиши I (*англ.*) на клавиатуре.

Но это ещё не весь её функционал. Если попробовать выбрать больше граней и нажать на **I** два раза, то получится иной результат, если бы мы нажали один раз. Каждая из граней получит свою вставку.

Это будет выглядеть так:

![Встака для каждой грани](./insertions/insert-for-all.gif)
***

## Масштабирование (Scale)
Масштабирование - это метод уменьшения либо увеличения объекта(-ов), грани(-ей), ребра(рёбер). Чтобы смаштабировать что-либо используют клавишу **S**.

Вот так оно выглядит:

![Масштабирование](./insertions/scale.gif)

Масштабирование полезно использовать опираясь на оси координат. Об этом чуть позже.
***

## Разрез (Loop cut)
По сути мы уже познакомились с двумя способами добавления вершин, но все они нарушали форму модели, так что изучим другой способ добавления вершин без нарушения целосности объекта.

Необходимо зажать **Ctrl** и нажать **R**, тогда, если направить курсор на объект, вы увидите линию разрезающую весь объект. Не спешите применять разрез, ведь вы можете увеличить его количество покрутив колёсиком мыши.

Вот как оно всё выглядит:

![Разрез](./insertions/loop-cut.gif)

Чтобы разрез добавился строго посередине меша, нужно сначала применить его **ЛКМ**, а после того как вы заметили, что разрез начал двигаться вместе с движением вашей мышки необходимо нажать **ПКМ**, это отменить движение линии разреза и поставит её в начальную позицию (*середину*).
***

## ПоОСЕвое моделирование
Прежде чем научиться другим функциям моделирования необходимо разобрать эту тему. Без данной темы вы будете тратить на много больше времени на создание модели, поэтому желательно освоить её полностью.

Поосевое моделирование работает практически на всех функциях в Blender.

На сцене вы видете две линии - одна жёлтая другая красная. Существует ещё третья линия - синия, но она видна только в отдельных случаях, когда вы повернёте камеру.

Все эти линии отвечают за свои **ОСИ** в 3D пространстве.

![Координатное пространство](./insertions/coordinate-system.png)

Давайте разберёмся где какая ось:
- Красная линия - ось **X**.
- Жёлтая линия  - ось **Y**.
- Синия линия   - ось **Z**.

Почти все функции в моделировании можно совершать *относительно* этих осей. 

Чтобы масштабировать грань относительно оси X необходимо выделить эту грань, нажать **S** и нажать на клавиатуре клавишу **оси X**, то есть просто клавишу **X**.

Выглядит это так:

![Масштабирование относительно Х](./insertions/scale-x.gif)

То есть после нажатия клавишы оси появляется линия того цвета, какую ось вы выбрали, советую запомнить цвета, чтобы потом быстро понимать относительно какой оси вы редактируете модель.

Да, поосевое моделирование работает почти с любой функцией, будь то **вставка** либо **выдавливание** или другие функции...

Но так же часто случаются моменты, когда нужно сделать что-либо относительно не одной оси, а сразу двух. В таких моментах логичным решением будет - нажать на кнопки двух осей одновременно. Однако это не сработает.

Чтобы сделать что-либо относительно **оси X** и **оси Z** необходимо зажать **Shift** и нажать на кнопку оси, которую хотим **убрать** - в данном случае **Shift + Y**.

![Масштабирование без Y](./insertions/scale-xz.gif)

Вам необходимо понять лишь логику данных действий, ведь поосевое моделирование очень полезно.
***

## Перемещение (Move)
Почему оно в самом низу? А всё потому что для перемещение практически всегда происходит относительно каких-либо осей. Если вы просто будете перемещать вершины, то у вас получится каша, а не модель, так что будьте аккуратнее.

Чтобы переместить что-либо необходимо нажать на клавишу **G**.

![Перемещение](./insertions/move.gif)
***

## Финальный акт
После изучения основных принципов построения модели нам необходимо использовать референс, простыми словами - чертеж, чтобы сделать микшер, повторяя контур чертежа теми способами, которые мы изучили ранее.

![Создание модели](./insertions/low-poly-model.png)

После изучения углубленных функций Blender'a будет предсатавлена глава, где будет подробно рассказываться как сделать часть микшера, опираясь на аддон MeasureIt для следования размерам и аддон Bool Tool, для вырезания отверстий. 
***

## Кратко
- Выделение вершин, рёбер, граней - **1, 2, 3. Shift + ЛКМ**. 
- Выдавливание **(Extrude) - E**.
- Вставка **(Insert) - I**.
- Месштабирование **(Scale) - S**.
- Добавление вершин **(Loop cut) - Ctrl + R**.
- Поосевое моделирование:
    1. **X, Y, Z** - относительно одной оси.
    2. **Shift + X, Y, Z** - без одной оси (относительно двух осей).
- Перемещение **(Move) - G**.
***

# Главы

[Следующая глава](./advance-hotkeys-to-building-models.md)
---

[Предыдущая глава](./introduction.md)
---

[Главная страница](../model-editing)