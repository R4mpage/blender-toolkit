# Алгоритм моделирования микшера часть два
Подробный гайд по созданию задней крышки микшера часть два представлен ниже.

## Подготовка
Согласно первой части алгоритма, вы уже имеете почти готовую заднюю крышку для микшера, однако в ней нет прорези для зарядного устройства. В данной части мы сделаем прорезь в крышке, чтобы можно было легко установить порт Micro-USB.

Вот как это будет выглядеть:

![Заготовка и готовая модель](./insertions/miksher-1-1.png)

## Начало
Как вы можете заметить, прорезь имеет квадратную форму и немного выходит вверх относительно всей наклонной плоскости крышки. Чтобы легче было работать с такой геометрией мы будем использовать отдельный объект, но сперва нам необходимо удалить ненужные грани на крышке.

С помощью функции разреза (**Ctrl + R**) создаем разрез посередине и отмечаем его расстояние аддоном **MeasureIt**. После перемещаем разрез до расстояния 52.55 мм.

*Примечание: из-за образовавшегося n-gon'а невозможно разрезать заднюю панель микшера целиком. Чтобы это сделать, необходимо зобавить разрез на нижнюю панель и верние рёбра, а после соединить соответствующие вершины путем их выделения и нажатия хоткея соединения (**Ctrl + J**).*

![Разрезы](./insertions/miksher-1-2.png)

Вот видео полного этапа работы:

![Часть 1](./insertions/miksher-1.gif)

## Ограничение наклонной
Так как коробка для порта USB находится не на полной области объекта, то необходимо восстановить часть наклонной. Для этого нужно выдавить ребро удаленной грани вверх на 0.58 мм.

![Возврат наклонной 1](./insertions/miksher-2-1.png)

После ещё раз выдавить ребро, только уже в сторону наклонной плоскости на расстояние 1.37 мм.

![Возврат наклонной 2](./insertions/miksher-2-2.png)

Когда небольшая коробка готова, необходимо привязать верние вершины к наклонной. Для этого потребуется добавить разрез на наклонную, а после, в настройках привязки выбрать пункт "Vertex".

![Возврат наклонной 3](./insertions/miksher-2-3.png)

После соединить разрезы с вершиной, прилегающей к наклонной.

![Возврат наклонной 4](./insertions/miksher-2-4.png)

*Примечание: после успешно проделанной работы верните привязку в режим **Increment**, чтобы впоследствии небыло проблем с моделированием* 

Последний пункт восстановления части наклонной - это заполнение пространства. Удалите конструкцию, которую только что создавали, так как она нужна лишь для обозначения разреза и выделите противоположные ребра. При нажатии на хоткей заполнения (**F**) область автоматически добавит грань на пустое место.

![Возврат наклонной финал](./insertions/miksher-2-5.png)

Вот видео полного этапа работы:

![Часть 2](./insertions/miksher-2.gif)

## Создание гнезда USB-порта

### Создание основы гнезда USB-порта
Гнездо под USB-порт легче всего сделать отдельно, поэтому продублируйте ребро (**Shift + D**), образующее пустое пространство, переместите подальше (желательно вверх используя привязку) и выдавите его вверх на 2.70 мм.

![Гнездо 1](./insertions/miksher-3-1.png)

Получилась пластина размером 2.70 мм х 13.90 мм. Следующим этапом необходимо выдавить грани в сторону наклонной поверхности крышки. Первая грань должна иметь глуюину 6.42 мм, а вторая 5.80 мм.

![Гнездо 2](./insertions/miksher-3-2.png)

Затем самое дальнее ребро необходимо выдавить вверх на 1.60 мм. После ещё раз выдавить внутрь на 0.60 мм и вверх на 0.60 мм.

![Гнездо 3](./insertions/miksher-3-3.png)

Заключительным промежуточным этапом станет формирование наклонных стенок на каждой из сторон фигуры. Нуобходимо выделить воответствующие вершины и нажать на кнопку формирования грани (**F**).

![Гнездо 4](./insertions/miksher-3-4.png)

Вот видео промежуточного этапа работы:

![Часть 3](./insertions/miksher-3.gif)

### Создание углубления гнезда USB-порта
Следующая цель - создание углубления в центре гнезда для USB-порта. Для этого необходимо добавить разрезы, дабы определить место углубления. Разрезы добавляются симметрично с обоих сторон гнезда.

Первый разрез - разрез по всей глубине гнезда с расстоянием от левой и правой границы 2.90 мм, Второй разрез 0.30 мм относительно первого.

Третий разрез должен находиться перпендикулярно глубине коробки гнезда и находиться на расстояние 5.11 мм от задней стенки.

![Гнездо 5](./insertions/miksher-4-1.png)

После того, как все разрезы сделаны, необходимо выдавить получившуюся область вниз на 2.50 мм, а затем, на нижнем ребре выдавить ещё одну область в глубину на 0.90 мм.

![Гнездо 6](./insertions/miksher-4-2.png)

Вот видео промежуточного этапа работы:

![Часть 4](./insertions/miksher-4.gif)

### Создание задней створки гнезда USB-порта
Для того, чтобы гнездо было частью задней панели микшера, необходимо добавить створку, которая будет частью основной панели. Для этого Нужно выдавить заднюю часть гнезда вниз на 0.20 мм, вверх на 2.00 мм, влево и вправо на 3.20 мм

![Гнездо 7](./insertions/miksher-5-1.png)

Вот видео промежуточного этапа работы:

![Часть 5](./insertions/miksher-5.gif)

### Создание скосов створки гнезда USB-порта
После того, как створка готова, её необходимо немного отредактировать, добавив скосы, чтобы при размещение Micro-USB не оставалось зазоров.

Относительно длины створки, верхняя часть скоса должна добавляться на 0.90 мм ближе к центру. Однако сужение начинается с 1.20 мм от высоты нижней поверхности.

![Гнездо 8](./insertions/miksher-6-1.png)

С противоположной стороны нужно сделать тоже самое, однако необходимо будет добавить разрез на 0.20 мм по высоте. После того, как части станут одинаковыми их необходимо соединить, используя хоткей формирования грани (**F**).

![Гнездо 9](./insertions/miksher-6-2.png)

Вот видео промежуточного этапа работы:

![Часть 6](./insertions/miksher-6.gif)

### Создание выемок гнезда USB-порта
Предзаключительным этапом моделирования гнезда является создание выемок. Выемка должа иметь цилинтрическую форму диаметром 1.8 мм, а также высоту размером 2.5 мм.

Примечание: добавьте на верхней грани буффер в высоту (около 0.1 - 0.2 мм), чтобы выдавливание цилиндрической формы прошло хорошо.

![Гнездо 10](./insertions/miksher-7-2.png)

После подготовки цилиндра необходимо определить центр выдавливания. Для этого необходимо добавить разрез в грубину на 2.80 мм относительно передней панели гнезда.

![Гнездо 11](./insertions/miksher-7-3.png)

Затем установите цилиндры согласно центрам пересечения разрезов и вырежите их из формы путем нажатия **Ctrl + Numpad(-)**.

Должен получиться такой результат:

![Гнездо 11](./insertions/miksher-7-4.png)

Вот видео промежуточного этапа работы:

![Часть 7](./insertions/miksher-7.gif)

### Создание гнезда USB-порта
Заключительным этаом является привязка этого гнезда к основной задней панели микшера. Для этого просто опускаем гнездо вниз до тех пор, пока все его вершины не встанут на свои места. 

*Примечание: если вы всегда использовали привязку при моделировании, то проблем возникнуть не должно.*

Финальный продукт - законченная задняя панель микшера:

![Гнездо 12](./insertions/miksher-final.png)

## Вывод
В следующей главе про алгоритмы будет рассказываться как создать переднюю панель микшера.

# Главы

[Следующая глава](../retopology)
---

[Предыдущая глава](./coordinate-surface.md)
---