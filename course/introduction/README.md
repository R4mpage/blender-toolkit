## Главы

[1. Введение в Blender](./introduction-in-blender.md)
---
[2. Первоначальная настройка](./first-customization.md)
---
[3. Смотр и управление интерфейсом](./first-look.md)
---
***

[Основы работы с моделями](../model-editing)